package launchd

import (
	"fmt"
	"strings"
)

const Header = ` <?xml version="1.0" encoding="UTF-8"?>
 <!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.    dtd">
 <plist version="1.0">
`

type Property struct {
	Key   string
	Value interface{}
}

type Plist struct {
	Label       string
	ProgramArgs []string
	KeepAlive   interface{}
	Properties  []Property
}

func NewPList(label string) *Plist {
	return &Plist{
		Label: label,
	}
}

func (p *Plist) AddProgramArguments(args []string) {
	p.ProgramArgs = args
}

func (p *Plist) Build() (string, error) {
	start := `	<dict>
<key>Label</key>
<string>` + p.Label + `</string>
`
	file := strings.Join([]string{Header, start}, "")
	//fmt.Println(fmt.Println(file))
	if len(p.ProgramArgs) > 0 {
		file = strings.Join([]string{file, "<key>ProgramArguments</key>"}, "")
		for _, arg := range p.ProgramArgs {
			str := "	<string>" + arg + "</string>"
			file = strings.Join([]string{file, str}, "\n")
		}
	}

	fmt.Println(file)
	return file, nil
}

//func NewPList(label string, args []string, keepAlive interface{}, props []Property) *Plist {
//	return &Plist{
//		Label:       label,
//		ProgramArgs: args,
//		KeepAlive:   keepAlive,
//		Properties:  props,
//	}
//}

func (pl *Plist) setStartInterval(seconds uint) {
	prop := Property{
		Key:   "StartInterval",
		Value: seconds,
	}

	pl.Properties = append(pl.Properties, prop)
}

func (pl *Plist) setCalendarInterval(minute uint, hour uint, day uint, weekday uint, month uint) {
	prop := Property{
		Key: "StartCalendarInterval",
		Value: []uint{
			minute,
			hour,
			day,
			weekday,
			month,
		},
	}

	pl.Properties = append(pl.Properties, prop)
}
