package main

import (
	"fmt"
	"launchd/launchd"
)

func main() {
	prop := launchd.Property{Key: "Test", Value: []string{"TestOne", "TestTwo", "TestThree"}}
	fmt.Println(prop)
}
